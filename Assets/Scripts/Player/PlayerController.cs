using System;
using UnityEngine;
using System.Collections.Generic;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;


namespace PointAndComa.Player
{
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class PlayerController : MonoBehaviour
    {
        public List<string> keys = new List<string>();
        [Tooltip("The movement speed")] [Range(0, 1)] public float Speed = 1;
        public Door currentDoor;
        public AudioClip spawnSound;
        public AudioClip stepSound;
        public AudioClip sfxCurse;
        public AudioClip sfxCurseOff;

        public GameObject curseOffParticles;
        public GameObject curseParticles;
        public GameObject spawnParticles;

		public bool isProtected;

        private ThirdPersonCharacter _character; // A reference to the ThirdPersonCharacter on the object
        private Vector3 _movement;
       
        private GameObject pickUp;
        private int selected;
        private List<PickUp> pickups;

       

        private void Start()
        {
            // get the third person character ( this should never be null due to require component )
            _character = GetComponent<ThirdPersonCharacter>();
            pickUp = null;
            pickups = new List<PickUp>();
            selected = 0;
        }

        void OnEnable()
        {
            AudioSource.PlayClipAtPoint(spawnSound, transform.position);
            Instantiate(spawnParticles, transform.position, Quaternion.identity);
        }

        private void Update()
        {
            if (CrossPlatformInputManager.GetButtonDown("Action"))
            {
                //if (pickUp != null) {
                //	pickups.Add (pickUp.GetComponent<PickUp> ());
                //	Destroy (pickUp);
                //	pickUp = null;

                //	if (pickups.Count == 1) {
                //		// TODO: Set image
                //	}

                //}

                GetComponent<Animator>().SetBool("Action", true);
            }

            if (CrossPlatformInputManager.GetButtonDown("Change"))
            {
                if (pickups.Count >= selected + 1)
                {
                    selected = 0;
                }
                else
                {
                    selected++;
                }
                // TODO: Set image
            }
        }

        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");

            // calculate move direction to pass to character
            _movement = v * Vector3.forward + h * Vector3.right;
            _movement.Normalize();
            _movement *= Speed;

            // pass all parameters to the character control script
            _character.Move(_movement, false, false);

            if (Input.GetKeyDown(KeyCode.L))
            {
                CurseOn();
            }
        }
        public void CurseOn()
        {
           
            Instantiate(curseParticles, transform.position, Quaternion.identity);
            
            AudioSource.PlayClipAtPoint(sfxCurse, transform.position);
        }

        public void CurseOff()
        {

            Instantiate(curseOffParticles, transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(sfxCurseOff, transform.position);
        }



        public void TurnOfAction()
        {
            if (currentDoor)
            {
                currentDoor.PlayLockSound();
                if (keys.Contains(currentDoor.doorKey))
                {
                    keys.Remove(currentDoor.doorKey);

                    currentDoor.Open();
                }
            }

            GetComponent<Animator>().SetBool("Action", false);
        }

        public void PlayStepSFX()
        {
            AudioSource.PlayClipAtPoint(stepSound, transform.position);
        }
    }
}