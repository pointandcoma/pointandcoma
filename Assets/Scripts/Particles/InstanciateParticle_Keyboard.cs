﻿using UnityEngine;
using System.Collections;

public class InstanciateParticle_Keyboard : MonoBehaviour {

    public GameObject[] particulas;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            Instantiate(particulas[0], transform.localPosition, transform.localRotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Instantiate(particulas[1], transform.localPosition, transform.localRotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Instantiate(particulas[2], transform.localPosition, transform.localRotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Instantiate(particulas[3], transform.localPosition, transform.localRotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            Instantiate(particulas[4], transform.localPosition, transform.localRotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            Instantiate(particulas[5], transform.localPosition, transform.localRotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            Instantiate(particulas[6], transform.localPosition, transform.localRotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            Instantiate(particulas[7], transform.localPosition, transform.localRotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            Instantiate(particulas[8], transform.localPosition, transform.localRotation);
        }
    }
}
