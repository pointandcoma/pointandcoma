﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (LoadScene ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	IEnumerator LoadScene ()
	{
		AsyncOperation a;

		a = SceneManager.LoadSceneAsync ("MainScene");
		a.allowSceneActivation = false;

		while (!a.isDone && a.progress < 0.89f) {
			Debug.Log ("loading" + a.progress);
			yield return null;
		}

		yield return new WaitForSeconds (3f);
		Debug.Log ("loaded");
		a.allowSceneActivation = true;

	}
}
