﻿using UnityEngine;
using System.Collections;

public class HudManager : MonoBehaviour {

	private static HudManager instance;

	public static HudManager Instance
	{
		get
		{
			return instance;
		}
	}

	public HUD_SCREENS currentScreen;
	public MonoBehaviour[] screens;



	void Awake() 
	{
		if (instance != null && instance != this) 
		{
			Destroy(this.gameObject);
			return;
		}
		else 
		{
			instance = this;
		}
	}
		

	public void ShowScreen (HUD_SCREENS screen)
	{
		for (int i = 0; i < screens.Length; i++) {

			if (screens [i].GetComponent<ScreenBase>().screenType == currentScreen) {
				screens [i].GetComponent<ScreenBase>().HideScreen ();
			}

			if (screens [i].GetComponent<ScreenBase>().screenType == screen) {
				screens [i].GetComponent<ScreenBase>().ShowScreen ();
			}
		}

	}

	public void CloseAllHUDS()
	{
		for (int i = 0; i < screens.Length; i++) {
			screens [i].GetComponent<ScreenBase>().HideScreen ();
		}

	}
}
