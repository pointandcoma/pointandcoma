﻿using UnityEngine;
using System.Collections;

public class ScreenBase : MonoBehaviour {

	public HUD_SCREENS screenType;

	public void ShowScreen ()
	{
		this.gameObject.SetActive (true);	
	}

	public void HideScreen ()
	{
		this.gameObject.SetActive (true);
	}

}
