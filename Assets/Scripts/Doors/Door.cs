﻿using System;
using UnityEngine;
using System.Collections;
using PointAndComa.Player;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(AudioSource))]
public class Door : MonoBehaviour
{
    public DOOR_STATES State = DOOR_STATES.DOOR_CLOSE;
    public string doorKey;

    public GameObject openParticles;
    public GameObject ligth;
    public Color colorLight;
    public AudioClip doorLockSFX;
    public AudioClip doorOpenSFX;


    private AudioSource AudioSource;
    private Animator Animator;



    private void Awake()
    {
        AudioSource=GetComponent<AudioSource>();
        if (AudioSource == null)
        {
            throw new ArgumentNullException("AudioSource", "AudioSource not found");
        }
        Animator = GetComponentInChildren<Animator>();
        if (Animator == null)
        {
            throw new ArgumentNullException("Animator", "Animator not found");
        }
        StartCoroutine(LightColor());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().currentDoor = this;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().currentDoor = null;
        }
    }

    public void PlayLockSound()
    {
        AudioSource.clip = doorLockSFX;
        AudioSource.Play();
    }

    public void Open()
    {
        AudioSource.clip = doorOpenSFX;
        AudioSource.Play();
        Animator.SetTrigger("Open");
        ligth.gameObject.SetActive(false);
        Instantiate(openParticles, transform.position, Quaternion.identity);

		if (doorKey == "1") {
			GameObject.Find ("GUI").SendMessage ("HideNotification");
		}
    }

    IEnumerator LightColor()
    {
        while (State == DOOR_STATES.DOOR_CLOSE)
        {
            float t = Mathf.PingPong(Time.time , 1.0f);
            ligth.GetComponent<Light>().color = Color.Lerp(Color.black, colorLight, t);
            yield return new WaitForEndOfFrame();
        }
    }
}