﻿using UnityEngine;
using System.Collections;

public class Hint : MonoBehaviour {



	public Transform target;

	public void FindTarget (string targetId)
	{
		GameObject obj = GameObject.Find ("Door" + targetId);
		if (obj) {
			target = obj.transform;
			GetComponent<NavMeshAgent> ().destination = target.position;
		}
	}

		
	}
