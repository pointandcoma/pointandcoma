﻿using UnityEngine;
using System.Collections;
using PointAndComa.Player;

public class Key : MonoBehaviour {

    public string keyID;
	public Color lightColor;

	public ParticleSystem particles_1;
	public ParticleSystem particles_2;
	public ParticleSystem particles_3;

    public GameObject destroyParticle;
	public GameObject doorHint;
    // Use this for initialization
	void Start () {



			}
	
	// Update is called once per frame
	void Update () {
		particles_1.startColor= lightColor;
		particles_2.startColor= lightColor;
		particles_3.startColor= lightColor;
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * 90f);
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == ("Player"))
        {
            other.GetComponent<PlayerController>().keys.Add(keyID);
            if(destroyParticle)
            {
                GetComponent<AudioSource>().Play();
                Instantiate(destroyParticle, transform.position, Quaternion.identity);
				GameObject obj = (GameObject)Instantiate(doorHint, transform.position, Quaternion.identity);

				obj.GetComponent<Hint> ().FindTarget (keyID);
            }
            Invoke("TurnOffGameObject", 0.5f);
        }
    }


    void TurnOffGameObject()
    {
		this.gameObject.SendMessage ("Notification");
        gameObject.SetActive(false);
    }
}
