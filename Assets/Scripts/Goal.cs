﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {

	public GameObject WinScreen;
	public AudioSource Music;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("You Win!");
		StartCoroutine (Winning ());
    }

	IEnumerator Winning()
	{
		Music.Stop ();
		WinScreen.SetActive (true);
		yield return new WaitForSeconds (12f);
		SceneManager.LoadScene (0);
	}

}
