﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class GUI_Behaviour : MonoBehaviour {

    [Header("GUI Menu Inicial")]
    public GameObject GUIMenuPrincipal;
    private Animator animMenuPrincipal;
    public bool inGame;
    public Animator animCamara;
	private GameObject player;
	public GameObject particleMarranada;

    [Header("GUI Menu Gameplay")]
    public GameObject GUIMenuGameplay;
    public GameObject PanelNotificaciones;
    public GameObject PanelPausa;
    public EventSystem EV_Gameplay;
    public string[] Mensajes;
    public bool enemyAreAlert;
	public AudioMixerSnapshot paused;
	public AudioMixerSnapshot unpaused;

    private bool isPaused;
    private bool isShowingNotification;
    private Animator animPanelNotificaciones;
    private Animator animPanelPausa;
    private Text txtMensaje;

    private bool timeNotification_Show;

    [Header("GUI Menu Gameplay")]
    public GameObject GUIMenuGameOver;

    private void Awake()
    {
        animMenuPrincipal = GUIMenuPrincipal.GetComponent<Animator>();

        animPanelNotificaciones = PanelNotificaciones.GetComponent<Animator>();
        animPanelPausa = PanelPausa.GetComponent<Animator>();
        txtMensaje = PanelNotificaciones.transform.GetChild(0).GetChild(0).GetComponent<Text>();
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        player.SetActive(false);
    }
		
	private void Update()
    {
        if(inGame)
        {
            if (Input.GetButtonDown("Pause"))
            {
                PauseGame();
            }
            /*
            if (Input.GetKeyDown(KeyCode.A))
            {
                ShowNotification(3);
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                HideNotification();
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                PlayerDead();
            }*/                      
        }
    }

    public void PlayGame()
    {
        StartCoroutine(Playing());
    }

    IEnumerator Playing()
    {
        animMenuPrincipal.SetTrigger("Iniciar");
        player.SetActive(false);
        yield return new WaitForSeconds(4f);

        animCamara.SetTrigger("Empezar");
        GUIMenuPrincipal.SetActive(false);
        GUIMenuGameplay.SetActive(true);
        yield return new WaitForSeconds(4f);

        Debug.Log("Aparecer usuario");
		player.SetActive(true);
        ShowNotification(1);

        yield return new WaitForSeconds(1f);
        
        inGame = true;
        Debug.Log("Inicia la partida, mueva la camara");
    }

    public void ShowNotificationTime(int numstep)
    {
        if(!timeNotification_Show)
        {
            timeNotification_Show = true;
            StartCoroutine(TimeNotification(numstep));
        }
    }

    IEnumerator TimeNotification(int numstep)
    {
        ShowNotification(numstep);

        yield return new WaitForSeconds(4);

        HideNotification();
        timeNotification_Show = false;
    }

    public void ShowNotification(int numStep)
    {
        if(isShowingNotification)
        {
            HideNotification();
        }

        animPanelNotificaciones.SetTrigger("Show");
        txtMensaje.text = Mensajes[numStep-1];
        isShowingNotification = true;
    }

    public void HideNotification()
    {
        isShowingNotification = false;
        animPanelNotificaciones.SetTrigger("Hide");
    }

    public void PauseGame()
    {
        if(!enemyAreAlert)
        {
            isPaused = !isPaused;
            animPanelPausa.SetBool("Pause", isPaused);

            EV_Gameplay.enabled = isPaused;

            if (isPaused)
            {
                Time.timeScale = 0;
				paused.TransitionTo (0.01f);
            }
            else
            {
                Time.timeScale = 1;
				unpaused.TransitionTo (0.01f);
			}
        }
        else
        {
            ShowNotificationTime(Mensajes.Length - 1);
        }
    }

    public void PlayerDead()
    {
        inGame = false;
        GUIMenuGameplay.SetActive(false);
        GUIMenuGameOver.SetActive(true);
    }

    public void RestartGame()
    {
        Debug.Log("Reinicia la partida");
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Debug.Log("Salir de la partida");
        Application.Quit();
    }
}
