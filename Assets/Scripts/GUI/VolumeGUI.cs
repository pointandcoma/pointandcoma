﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VolumeGUI : MonoBehaviour {

    private AudioSource[] AllAudios;
    private float[] AllVolumeConstant;
    private Scrollbar VolumeScroll;
	// Use this for initialization
	void Awake ()
    {
        AllAudios = GameObject.FindObjectsOfType(typeof(AudioSource)) as AudioSource[];

        if(AllAudios.Length > 1)
        {
            AllVolumeConstant = new float[AllAudios.Length];

            for (int i = 0; i < AllAudios.Length; i++)
            {
                AllVolumeConstant[i] = AllAudios[i].volume;
            }
        }
        
        VolumeScroll = GetComponent<Scrollbar>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(AllAudios.Length > 1)
        {
            for (int i = 0; i < AllAudios.Length; i++)
            {
                AllAudios[i].volume = AllVolumeConstant[i] * VolumeScroll.value;
            }
        }
    }
}
