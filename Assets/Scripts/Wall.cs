﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GetComponentInParent<Animator>().SetTrigger("Open");
        }
    }
}