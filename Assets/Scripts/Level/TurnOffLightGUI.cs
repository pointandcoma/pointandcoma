﻿using UnityEngine;
using System.Collections;

public class TurnOffLightGUI : MonoBehaviour {

    public float minTime;
    public float maxTime;

    private Light light;
    private float timeSwitch;
	// Use this for initialization
	void Start ()
    {
        light = GetComponent<Light>();

        timeSwitch = Random.Range(minTime, maxTime);
	}
	
	// Update is called once per frame
	void Update ()
    {
        timeSwitch -= Time.deltaTime;

	    if(timeSwitch <= 0)
        {
            SwitchLight();
            timeSwitch = Random.Range(minTime, maxTime);
        }
    }

    void SwitchLight()
    {
        if(light.intensity == 1.2f)
        {
            light.intensity = 0.3f;
            return;
        }
        else if (light.intensity == 0.3f)
        {
            light.intensity = 1.2f;
            return;
        }
    }
}
