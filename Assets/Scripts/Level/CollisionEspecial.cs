﻿using UnityEngine;
using System.Collections;

public class CollisionEspecial : MonoBehaviour {

    public Animator animPared;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            animPared.SetTrigger("Abrir");

            Destroy(this.gameObject);
        }
    }
}
