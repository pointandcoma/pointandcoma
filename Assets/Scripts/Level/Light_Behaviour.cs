﻿using UnityEngine;
using System.Collections;
using PointAndComa.Player;

public class Light_Behaviour : MonoBehaviour {

    public bool isATrap;

	public bool isPlayerProtected;

    private Light spotLight;
    private Light pointLight;
    private float firstIntensity_Spot;
    private float firstIntensity_Point;

    private bool changeLight;
    private bool lightOff;
    private float percent = 100;

	private SphereCollider myCollider;

	private void Awake()
    {
        pointLight = transform.GetChild(0).GetComponent<Light>();
        spotLight = transform.GetChild(1).GetComponent<Light>();

        firstIntensity_Point = pointLight.intensity;
        firstIntensity_Spot = spotLight.intensity;

		myCollider = GetComponent<SphereCollider> ();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.L))
        {
            ChangeLight();
        }
        
	    if(changeLight)
        {
            if(lightOff)
            {
                percent -= Time.deltaTime * 60;

                if (percent > 40)
                {
                    pointLight.intensity = percent * firstIntensity_Point / 100;
                    spotLight.intensity = percent * firstIntensity_Spot / 100;
                }
                else
                {
                    pointLight.intensity = 40 * firstIntensity_Point / 100;
                    spotLight.intensity = 40 * firstIntensity_Spot / 100;

                    percent = 40;

                    changeLight = false;
                }
            }
            else
            {
                percent += Time.deltaTime * 60;

                if (percent < 100)
                {
                    pointLight.intensity = percent * firstIntensity_Point / 100;
                    spotLight.intensity = percent * firstIntensity_Spot / 100;
                }
                else
                {
                    pointLight.intensity = firstIntensity_Point;
                    spotLight.intensity = firstIntensity_Spot;

                    percent = 100;

                    changeLight = false;
                }
            }
        }
	}

    public void ChangeLight()
    {
        changeLight = true;
        lightOff = !lightOff;
		myCollider.enabled = !lightOff;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (isATrap)
            {
                pointLight.enabled = false;
                spotLight.enabled = false;
            }
            else
            {
				other.gameObject.GetComponent<PlayerController> ().isProtected = true;
                Debug.Log("Usuario Protegido por luz");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
			other.gameObject.GetComponent<PlayerController> ().isProtected = false;
            Debug.Log("Usuario desprotegido!");
        }
    }
}
