﻿using UnityEngine;
using System.Collections;

public class Music_Behaviour : MonoBehaviour {

    public AudioClip musicStealth;
    public AudioClip musicAlert;
    private AudioSource music;
    private bool Alert;

    private static Music_Behaviour instance;

    public static Music_Behaviour Instance
    {
        get
        {
            return instance;

        }

    }


    // Use this for initialization
    void Awake ()
    {
        if (!instance)
        {
            instance = this;
        }
        
        music = GetComponent<AudioSource>();
	}

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.M))
        {
            ChangeMusic();
        }
    }

    public void ChangeMusic()
    {
        Alert = !Alert;

        music.Stop();

        if(Alert)
        {
            music.clip = musicAlert;
        }
        else
        {
            music.clip = musicStealth;
        }

        music.Play();
    }
}
