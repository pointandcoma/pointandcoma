﻿using UnityEngine;
using System.Collections;

public class NotificationCollider : MonoBehaviour {

    [Header("Tipo de Collider")]
    public int numMessage;
    public bool showMessage;
    public bool showMessageTime;
    public bool hideMessage;
	public bool possible = false;

    public bool disableAtTrigger = true;

    private GUI_Behaviour GUI;

    private void Awake()
    {
        GUI = GameObject.Find("GUI").GetComponent<GUI_Behaviour>();
    }

	private void OnTriggerEnter(Collider Other)
	{
		if (Other.tag == "Player" && possible) 
		{
			Notification ();
		}
	}

	private void Notification()
    {
    	if (showMessage)
        {
            GUI.ShowNotification(numMessage);
        }
        else if (hideMessage)
        {
            GUI.HideNotification();
        }
        else if (showMessageTime)
        {
            GUI.ShowNotificationTime(numMessage);
        }

		if(disableAtTrigger)
        {
            Destroy(this.gameObject);
        }
    }
}
