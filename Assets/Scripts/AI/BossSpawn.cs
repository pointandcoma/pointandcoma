﻿using System;
using UnityEngine;
using System.Collections;

namespace PointAndComa.AI
{
    public class BossSpawn : MonoBehaviour
    {
        [Tooltip("The boss")] public AIBossController Boss;

        private void Reset()
        {
            Boss = gameObject.GetComponentInChildren<AIBossController>();
        }

        private void Awake()
        {
            if (Boss == null)
            {
                Boss = gameObject.GetComponentInChildren<AIBossController>();

                if (Boss == null)
                {
                    throw new ArgumentNullException("Boss", "Boss not found");
                }
            }
        }

        private void Start()
        {
            Boss.gameObject.SetActive(false);
        }

        public void SpawnBoss()
        {
            if (!Boss.gameObject.activeSelf)
            {
                EnemiesManager.Instance.EnemiesOff();
                Boss.gameObject.SetActive(true);
            }
        }
    }
}