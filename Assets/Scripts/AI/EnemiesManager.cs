﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PointAndComa.AI;
using PointAndComa.Player;

public class EnemiesManager : MonoBehaviour {

	private static EnemiesManager instance;

	public int numAlertEnemy = 0;
	public int numAlertBoss = 0;
  

    private bool isInAlertMode;
    public GameObject player;
	public Light_Behaviour[] lights;

	public static EnemiesManager Instance
	{
		get{
			return instance;
		}
	}


	public List<AIEnemyController> enemies = new List<AIEnemyController>();
	void Awake ()
	{
		if (!instance) {

			instance = this;
			
		}

		lights = FindObjectsOfType (typeof(Light_Behaviour)) as Light_Behaviour[];
	}



	// Use this for initialization
	void Start () {
        if (!player)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
	}

	// Update is called once per frame
	void Update () {
	


	}


	public void EnemiesOff ()
	{
		foreach (AIEnemyController enemy in enemies) {
			enemy.gameObject.SetActive (false);
		}

	}

	public void EnemiesON ()
	{
		foreach (AIEnemyController enemy in enemies) {

			if (enemy._state != AIEnemyStates.Dead) {
				enemy.gameObject.SetActive (true);
			}


		}

	}


	public void ListenAlert ()
	{
		bool areEnemiesSeeking = false;
		foreach (AIEnemyController enemy in enemies) {
			areEnemiesSeeking = (enemy._state == AIEnemyStates.Chasing);
		}

		if (areEnemiesSeeking) {
			AlertOn ();
		} 
		else {
			AlertOff ();
		}
	}

	private void AlertOn()
	{
		if (!isInAlertMode) {
            Music_Behaviour.Instance.ChangeMusic();
            
            // Cambiar Luces
			foreach (Light_Behaviour l in lights) {
				l.ChangeLight ();
			}

            Camera.main.gameObject.SendMessage ("ChangeEffect", SendMessageOptions.DontRequireReceiver);
			// Poner sonido luces apagadas

			isInAlertMode = true;
		}
	}

	private void AlertOff()
	{
        Music_Behaviour.Instance.ChangeMusic();
       // player.GetComponent<PlayerController>().CurseOff();
        
		// Cambiar Luces
		foreach (Light_Behaviour l in lights) {
			l.ChangeLight ();
		}

        Camera.main.gameObject.SendMessage ("ChangeEffect", SendMessageOptions.DontRequireReceiver);
		// Poner sonido luces prendidas

		isInAlertMode = false;
	}
}
