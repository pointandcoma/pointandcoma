﻿using System;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

namespace PointAndComa.AI
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class AIBossController : MonoBehaviour
    {
        [Tooltip("The life spawn")] public float LifeSpan = 30f;
        public GameObject spawnParticles;
        public AudioClip sfxStep;
        public AudioClip sfxSpawn;

        private NavMeshAgent _agent;
        private ThirdPersonCharacter _character;
        
        private Vector3 _startPosition;
        private float _remainingLifeSpan;
        private Transform _player;

        private void Awake()
        {
            if (LifeSpan < 0)
            {
                throw new ArgumentOutOfRangeException("LifeSpan", LifeSpan, "LifeSpan should be positive");
            }

            _agent = GetComponentInChildren<NavMeshAgent>();
            _character = GetComponent<ThirdPersonCharacter>();

            _player = GameObject.FindGameObjectWithTag("Player").transform;
            if (_player == null)
            {
                throw new ArgumentNullException("Player", "Player should be tagged as 'Player'");
            }

            _startPosition = transform.position;
        }

        private bool isFirtsEnable;
        private void OnEnable()
        {
            transform.position = _startPosition;
            
                Instantiate(spawnParticles, transform.position, Quaternion.identity);
                AudioSource.PlayClipAtPoint(sfxSpawn, transform.position); 
            
        }

        private void Start()
        {
            _remainingLifeSpan = LifeSpan;
            isFirtsEnable = true;
            _agent.updateRotation = false;
            _agent.updatePosition = true;
        }

        private void Update()
        {
            // Reduce lifespan
            _remainingLifeSpan -= Time.deltaTime;
            if (_remainingLifeSpan < 0)
            {
				EnemiesManager.Instance.EnemiesON ();
				EnemiesManager.Instance.ListenAlert ();
                gameObject.SetActive(false);
                return;
            }

            // Player reached
            if (Vector3.Distance(transform.position, _player.position) < _agent.stoppingDistance)
            {
                OnPlayerReached();
                return;
            }

            // Chase player
            _agent.SetDestination(_player.position);

            // Update animator
            if (_agent.remainingDistance > _agent.stoppingDistance)
            {
                _character.Move(_agent.desiredVelocity, false, false);
            }
            else
            {
                _character.Move(Vector3.zero, false, false);
            }
        }

        private void OnPlayerReached()
        {
            // TODO end game
            

            _player.gameObject.SetActive(false);

			GameObject.FindGameObjectWithTag ("UI").GetComponent<GUI_Behaviour> ().PlayerDead ();
        }

        public void PlayStepSFX()
        {
            AudioSource.PlayClipAtPoint(sfxStep, transform.position);
        }
    }
}