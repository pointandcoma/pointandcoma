using PointAndComa.Player;
using System;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

namespace PointAndComa.AI
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class AIEnemyController : MonoBehaviour
    {
        [Tooltip("The field of view in degrees")] [Range(0, 360)] public float FoV = 30f;
        [Tooltip("The eyes offset")] public Vector3 EyesOffset = new Vector3(0, 1.33f, 0);
        [Tooltip("The maximum view distance")] public float ViewDistance = 10;
        [Tooltip("The maximum hear distance")] public float HearDistance = 2.5f;
        [Tooltip("The patrol speed")] [Range(0, 1)] public float PatrolSpeed = .5f;
        [Tooltip("The chasing speed")] [Range(0, 1)] public float ChasingSpeed = .75f;
        [Tooltip("The patrol target")] public Transform PatrolTarget;
        [Tooltip("The boss spawn this enemy belongs to")] public BossSpawn BossSpawn;

        private NavMeshAgent _agent;
        private ThirdPersonCharacter _character;

        public AIEnemyStates _state;
        private Transform _player;
        public AudioClip sfxStep;

        private void Awake()
        {
            _agent = GetComponentInChildren<NavMeshAgent>();
            _character = GetComponent<ThirdPersonCharacter>();

            if (BossSpawn == null)
            {
                BossSpawn = gameObject.GetComponentInParent<BossSpawn>();
                if (BossSpawn == null)
                {
                    throw new ArgumentNullException("BossSpawn", "BossSpawn not found");
                }
            }

            _player = GameObject.FindGameObjectWithTag("Player").transform;
            if (_player == null)
            {
                throw new ArgumentNullException("Player", "Player should tagged as 'Player'");
            }
        }


        private void Start()
        {
            _agent.updateRotation = false;
            _agent.updatePosition = true;
            _agent.speed = PatrolSpeed;
            _state = AIEnemyStates.Patrol;

			EnemiesManager.Instance.enemies.Add (this);
        }

        private void Update()
        {
            // Update the current state and act accordingly
            if (UpdateState())
            {
                OnStateChange();
            }

            // Execute the state
            ExecuteState();

            // Player reached
            if (Vector3.Distance(transform.position, _player.position) < _agent.stoppingDistance)
            {
                OnPlayerReached();
                return;
            }

            // Update animator
            if (_agent.remainingDistance > _agent.stoppingDistance)
            {
                _character.Move(_agent.desiredVelocity, false, false);
            }
            else
            {
                _character.Move(Vector3.zero, false, false);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, HearDistance);
            Gizmos.DrawWireSphere(transform.position, ViewDistance);
            Gizmos.DrawLine(transform.position,
                transform.position + Quaternion.Euler(0, FoV, 0) * transform.forward * ViewDistance);
            Gizmos.DrawLine(transform.position,
                transform.position + Quaternion.Euler(0, -FoV, 0) * transform.forward * ViewDistance);

            if (Application.isPlaying && CanSeePlayer())
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position + EyesOffset, _player.position + EyesOffset);
            }
        }

        private bool UpdateState()
        {
            switch (_state)
            {
                case AIEnemyStates.Patrol:
                    if (CanSeePlayer())
                    {
                        _state = AIEnemyStates.Chasing;
                        return true;
                    }
                    break;
                case AIEnemyStates.Chasing:
                    if (!CanSeePlayer())
                    {
                        _state = AIEnemyStates.Lost;
                        return true;
                    }
                    break;
                case AIEnemyStates.Lost:
                    if (CanSeePlayer())
                    {
                        _state = AIEnemyStates.Chasing;
                        return true;
                    }
                    else if (_agent.remainingDistance < _agent.stoppingDistance)
                    {
                        _state = AIEnemyStates.Patrol;
                        return true;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return false;
        }

        private void OnStateChange()
        {
            switch (_state)
            {
                case AIEnemyStates.Patrol:
                    _agent.speed = PatrolSpeed;
                    break;
			case AIEnemyStates.Chasing:
				EnemiesManager.Instance.ListenAlert ();
                    _agent.speed = ChasingSpeed;
                    break;
                case AIEnemyStates.Lost:
				EnemiesManager.Instance.ListenAlert ();

                    _agent.speed = ChasingSpeed;
                    _agent.SetDestination(_player.position);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ExecuteState()
        {
            switch (_state)
            {
                case AIEnemyStates.Patrol:
                    if (PatrolTarget != null)
                        _agent.SetDestination(PatrolTarget.position);
                    break;
                case AIEnemyStates.Chasing:
                    _agent.SetDestination(_player.position);
                    break;
                case AIEnemyStates.Lost:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private bool CanSeePlayer()
        {
            Vector3 toPlayer = _player.position - transform.position;

			if (_player.gameObject.GetComponent<PlayerController>().isProtected) {

				return false;
			}

            // Is near enough
            if (toPlayer.magnitude < HearDistance)
            {
                // Has a clear view of the player
                return RaycastToPlayer(toPlayer);
            }

            // Is far enough
            if (toPlayer.magnitude > ViewDistance)
            {
                return false;
            }

            // Is in the FoV
            if (Vector3.Angle(transform.forward, toPlayer) > FoV)
            {
                return false;
            }



            // Has a clear view of the player
            return RaycastToPlayer(toPlayer);
        }

        private bool RaycastToPlayer(Vector3 toPlayer)
        {
            // The player should be in the "Ignore Raycast" layer
            return !Physics.Raycast(transform.position + EyesOffset, toPlayer, toPlayer.magnitude);
        }

        private void OnPlayerReached()
        {
            BossSpawn.SpawnBoss();
			_state = AIEnemyStates.Dead;
            _player.GetComponent<PlayerController>().CurseOn();
            //Maldiciones Player :v
            EnemiesManager.Instance.enemies.Remove (this);
            gameObject.SetActive(false);
        }

        public void PlayStepSFX()
        {
           // AudioSource.PlayClipAtPoint(sfxStep, transform.position, 0.25f);
        }

    }

    public enum AIEnemyStates
    {
        Patrol,
        Chasing,
        Lost,
		Dead
    }
}